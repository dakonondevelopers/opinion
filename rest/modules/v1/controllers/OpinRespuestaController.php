<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use app\models\Opin;
use app\models\OpinStat;
use app\models\OpinRespuesta;
use app\models\OpinRespuestaAbierta;


class OpinRespuestaController extends Controller
{

	public $modelClass = 'app\models\OpinRespuesta';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className()],
        ];
    }

    /**
     * Método para enviar una o varias respuestas en una opin
     * @author Rodrigo Boet
     * @date 06/11/2016
     * @param $id_opin Recibe el id de la opin
     * @param $token Recibe el token si la opin es cerrada
     * @return string Retorna mensajes o errores
     */
    public function actionParticipar($id_opin,$token='')
    {
        $model = Opin::findOne($id_opin);
        if($model)
        {
            $data = Yii::$app->request->post();
            if(!$model->publica and $token=='')
            {
                return ['validacion'=>'error', 'mensaje'=> 'You must send a token'];
            }
            else if(!$model->publica and $model->opinPrivada->token!=$token)
            {
                return ['validacion'=>'error', 'mensaje'=> 'Token invalid'];
            }
            //Si la respuesta proviene del usuario que creo la opin regresa error
            if($model->fk_user==Yii::$app->user->id)
            {
                return ['validacion'=>'error', 'mensaje'=> 'You cannot answer your own opin'];
            }
            //Se consultan las respuestas tanto abiertas como cerradas
            $opin_respuesta = OpinRespuesta::find()->joinWith('fkOpcion')
            ->where(['opin_opcion.fk_opin'=>$id_opin,'fk_user'=>Yii::$app->user->id])->one();
            $opin_respuesta_abierta = OpinRespuestaAbierta::find()->joinWith('fkOpcion')
            ->where(['opin_opcion.fk_opin'=>$id_opin,'fk_user'=>Yii::$app->user->id])->one();
            //Si el usuario ya respondio a esta opin envia un mensaje de aviso
            if($opin_respuesta or $opin_respuesta_abierta)
            {
                return ['validacion'=>'error', 'mensaje'=> 'You have already send answer to this opin']; 
            }
            //Opción Simple
            if ($model->seleccion == 0)
            {
                if(isset($data["fk_opcion[]"]))
                {
                    return ['validacion'=>'error', 'mensaje'=> 'You cannot send multiple answer'];
                }
                $respuesta = new OpinRespuesta();
                $respuesta->setAttributes(Yii::$app->request->post());
                $respuesta->fk_user = Yii::$app->user->id;
                $respuesta->fecha_respuesta = date('Y-m-d');
                $validar = $this->validar($model,Yii::$app->request->post());
                if($respuesta->validate() and $validar)
                {
                    //Se crea el modelo para la estadistica
                    $opin_stat = new OpinStat();
                    $opin_stat->fk_user = $respuesta->fk_user;
                    $opin_stat->fk_opin = $id_opin;
                    $opin_stat->save();
                    $respuesta->save();
                    return ['validacion'=>'ok', 'mensaje'=> 'Answer successfully submited'];
                }
                else if(!$validar)
                {
                    return ['validacion'=>'error', 'mensaje'=> 'Option not in opin'];
                }
                else
                {
                    return ['validacion'=>'error', 'mensaje'=> $respuesta->getErrors()];
                }
            }
            //Opcion Múltiple
            else if($model->seleccion == 1)
            {
                if(!isset($data["fk_opcion[]"]))
                {
                    return ['validacion'=>'error', 'mensaje'=> 'You must send at least one answer'];
                }
                else
                {
                    $data = $this->toArray(Yii::$app->request->post(),'fk_opcion[]');
                    $validar = $this->validar($model,$data);
                    if(!$validar)
                    {
                        return ['validacion'=>'error', 'mensaje'=> 'Option not in opin'];
                    }
                    else
                    {
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            foreach ($data as $value) {
                                $respuesta = new OpinRespuesta();
                                $respuesta->fk_opcion = $value;
                                $respuesta->fk_user = Yii::$app->user->id;
                                $respuesta->fecha_respuesta = date('Y-m-d H:m:s');
                                if (! ($flag = $respuesta->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                            if ($flag) {
                                $transaction->commit();
                                //Se crea el modelo para la estadistica
                                $opin_stat = new OpinStat();
                                $opin_stat->fk_user = Yii::$app->user->id;
                                $opin_stat->fk_opin = $id_opin;
                                $opin_stat->save();
                                return ['validacion'=>'ok', 'mensaje'=> 'answer successfully submited'];
                            }
                        } catch (Exception $e) {
                            $transaction->rollBack();
                        }
                    }
                }
            }
        }
        else
        {
            return ['validacion'=>'error', 'mensaje'=> 'Cannot find Opin"'];
        }
    }

    /**
     * Método para validar que los id's de las opciones correspondan a la opin solicitada
     * @author Rodrigo Boet
     * @date 06/11/2016
     * @param $model Recibe el modelo de la opin
     * @param $elements Recibe un array con los elementos
     * @return boolean retorna Verdadero/Falso
     */
    public function validar($model,$elements)
    {
        $opciones = $model->opinOpcions;
        $keys = array_keys(ArrayHelper::map($opciones, 'id_opcion', 'id_opcion'));
        $elements = array_values($elements);
        $values = array_intersect($elements, $keys);
        if(count($values)>0)
        {
            return true;
        }
        return false;
    }

    /**
     * Método para transformar un array enviado a POST a uno normal
     * @author Rodrigo Boet
     * @date 06/11/2016
     * @param $elements Recibe un arreglo con los elementos
     * @param $elemnameents Recibe el nombre de la llave que se desea transformar
     * @return array retorna el Arreglo
     */
    public function toArray($elements,$name)
    {
        $new_array = [];
        foreach ($elements[$name] as $value) {
            array_push($new_array, $value);
        }
        return $new_array;
    }
    
}