<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /*
    *   Funcion que permite cargar una foto
    *   Creado por: Rodrigo Boet
    *   Fecha: 07/09/2016
    *   @return str Se regresa el nombre del archivo o el error
    */
    public function actionUploadPicture()
    {
        $ui = \yii\web\UploadedFile::getInstance($model, 'opin_foto');
        if($ui)
        {
            $name = Yii::$app->user->id.Yii::$app->security->generateRandomString().'.'.split('/',$ui->type)[1];
            $ui->saveAs(Yii::getAlias('@webroot') . '../web/img/uploads/ ' . $name);
            $file= '/uploads/ ' . $name;
            return $file;
        }
        else
        {
            return "error uploading picture";
        }
    }
}
