<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;

class PerfilController extends ActiveController
{

	public $modelClass = 'app\models\Perfil';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className()],
        ];
    }
	
	public function actions()
	{
		$actions = parent::actions();
		// disable the "delete","create" actions
		unset($actions['delete'],$actions['create']);
		return $actions;
	}

}
