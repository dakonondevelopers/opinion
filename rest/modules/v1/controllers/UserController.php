<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use app\forms\RegisterForm;
use app\forms\LoginForm;
use app\forms\PasswordResetRequestForm;
use app\models\User;
use app\models\Perfil;

class UserController extends ActiveController
{

	public $modelClass = 'app\models\User';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
	/**
	 * Función donde se definen las acciones estándar del modelo
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "create" actions
		unset($actions['delete'], $actions['create']);

		return $actions;
	}
	
	/**
	 * Función para crear el usuario
	 * @author Rodrigo Boet
	 * @date 22/10/2016
	 */
	public function actionCreate()
	{
		$register = new RegisterForm();
		$register->setAttributes(Yii::$app->request->post());
		$message = '';
		if($register->validate())
		{
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$user = new User();
	            $user->username = $register->username;
	            $user->email = $register->email;
				$user->setPassword($register->password);
	            $user->generateAuthKey();
	            $user->status = 0;
	            $user->save();
	            //perfil
	            $perfil = new Perfil();
	            $perfil->telefono = $register->telefono;
	            $perfil->genero = $register->genero;
	            $perfil->edad = $register->edad;
	            $perfil->fecha_nacimiento = $register->fecha_nacimiento;
	            $perfil->fk_user = $user->id;
	            $perfil->fk_pais = $register->pais;
	            $perfil->save();
	            //Se asigna el rol del usuario
	            $auth = Yii::$app->authManager;
	            $authorRole = $auth->getRole('USUARIO');
	            $auth->assign($authorRole, $user->id);
	            //Se envia el correo
	            Yii::$app->mailer
	            ->compose(
	                ['html' => 'register-html', 'text' => 'register-text'],
	                ['user' => $user, 'perfil' => $perfil]
	            )
	            ->setTo($user->email)
	            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["Title"]])
	            ->setSubject($user->username)
	            ->send();
	            $transaction->commit();
            	$message = ['validacion'=>'ok','mensaje'=>'Mail has send, check your email'];
            } catch (\Swift_TransportException $e) {
                $transaction->rollBack();
                $message = ['validacion'=>'error','mensaje'=>print_r($e)];
            }

		}
		else
		{
			$message = ['validacion'=>'error','mensaje'=>$register->getErrors()];
		}
		return $message;
	}
	
	/**
	 * Función para logearse
	 * @author Rodrigo Boet
	 * @date 22/10/2016
	 */
	public function actionLogin()
	{
		$login = new LoginForm();
		$login->setAttributes(Yii::$app->request->post());
		$message = '';
		if($login->validate())
		{
			$message = ['validacion'=>'ok','mensaje'=>'Success',
			'auth_key'=>$login->getUser()->auth_key];
		}
		else
		{
			$message = ['validacion'=>'error','mensaje'=>$login->getErrors()];
		}
		return $message;
	}

	/**
	 * Función para resetear el password
	 * @author Rodrigo Boet
	 * @date 22/10/2016
	 */
	public function actionResetPassword()
    {
        $model = new PasswordResetRequestForm();
        $message = '';
        $model->setAttributes(Yii::$app->request->post());
        if ($model->validate()) {
            if ($model->sendEmail()) {
            	$message = ['validacion'=>'ok','mensaje'=>'Check your email to recover password.'];
            } else {
            	$message = ['validacion'=>'error','mensaje'=>'Sorry, cannot send email to recover password.'];
            }
        }
        else
        {
        	$message = ['validacion'=>'error','mensaje'=>$model->getErrors()];
        }
        return $message;

    }
}
