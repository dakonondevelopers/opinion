<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;
use app\rest\models\OpinSearchPublica;
use app\models\OpinSearch;
use app\models\Opin;
use app\models\OpinOpcion;
use app\models\OpinPrivada;
use app\models\Perfil;
use app\forms\OpinForm;
use app\controllers\OpinController as OpinMethod;
use app\rest\modules\v1\controllers\OpinRespuestaController;


class OpinController extends ActiveController
{

	public $modelClass = 'app\models\Opin';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className()],
        ];
    }
    
	/**
	 * Función donde se definen las acciones estándar del modelo
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete", "create" and "view" actions
		unset($actions['delete'], $actions['create'], $actions['view'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

		return $actions;
	}

    /**
     * Método para filtrar en el servicio del usuario
     */
    public function prepareDataProvider()
    {
        $model = new OpinSearchPublica();

        return $model->search(Yii::$app->request->queryParams);
    }

    /**
     * Método para crear una opin y todas sus opciones
     * @author Rodrigo Boet
     * @date 05/11/2016
     * @return string Retorna mensajes
     */
    public function actionCreate()
    {
        $model = new OpinForm();
        $data = Yii::$app->request->post();
        $model->setAttributes($data);
        $transaction = Yii::$app->db->beginTransaction();
        $user = Perfil::find()->where(['fk_user'=>Yii::$app->user->id])->one();
        $model->fk_user = $user->fk_user;
        $model->fk_idioma = $user->fkPais->idioma;
        $model->fecha_creacion = date('Y-m-d');
        if(!isset($data["texto_opcion[]"]))
        {
            return ['validacion'=>'error', 'mensaje'=> 'You must send at least one option'];
        }
        else if($model->validate())
        {
            $opin = new Opin();
            OpinMethod::copyValues($opin,$model);
            try {
                if ($flag = $opin->save(false)) {
                    //Si la sala es privada se genera el token de forma automática
                    if(!$opin->publica)
                    {
                        $privada = new OpinPrivada();
                        $privada->token = Yii::$app->user->id.Yii::$app->security->generateRandomString();
                        $privada->fk_opin = $opin->id_opin;
                        $privada->save();
                    }
                    //Se crea la tematica
                    OpinMethod::getTematica($data['tematica[]'],$opin->id_opin);
                    foreach ($data["texto_opcion[]"] as $opciones) {
                        $opcion = new OpinOpcion();
                        $opcion->texto_opcion = $opciones;
                        $opcion->fk_opin = $opin->id_opin;
                        if (! ($flag = $opcion->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }
                }
                if ($flag) {
                    $transaction->commit();
                    return ['validacion'=>'ok', 'mensaje'=>'opin successfully created'];
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
        else
        {
            return ['validacion'=>'error', 'mensaje' => $model->getErrors()];
        }
    }

    /**
     * Método para actualizar una opin y todas sus opciones
     * @author Rodrigo Boet
     * @date 12/11/2016
     * @return string Retorna mensajes
     */
    public function actionUpdate($id)
    {
        $model = Opin::findOne($id);
        $opciones = $model->opinOpcions;
        $model->setAttributes(Yii::$app->request->post());
        $data = Yii::$app->request->post();
        if(isset($data["id_opcion[]"]) and isset($data["texto_opcion[]"]))
        {
            $id_opcion = OpinRespuestaController::toArray(Yii::$app->request->post(),'id_opcion[]');
            $texto_opcion = OpinRespuestaController::toArray(Yii::$app->request->post(),'texto_opcion[]');
            $validar = OpinRespuestaController::validar($model,$id_opcion);
            if(!$validar)
            {
                return ['validacion'=>'error', 'mensaje'=> 'Option not in opin'];
            }
            else
            {
                OpinMethod::deleteTematica($model,$data["tematica[]"]);
                foreach ($opciones as $opcion) {
                    $flag = false;
                    foreach ($id_opcion as $key => $value) {
                        if($opcion->id_opcion==$value)
                        {
                            $flag = true;
                            $opcion->texto_opcion = $texto_opcion[$key];
                            $opcion->save();
                        }
                    }
                    if(!$flag)
                    {
                        $opcion->delete();
                    }
                }
                return ['validacion'=>'ok','mensaje'=>'Opin updated successfully'];
            }
        }
    }

    /**
     * Método para ver las opiniones privadas creadas por usuario logeado
     * @author Rodrigo Boet
     * @date 06/11/2016
     * @return ModelSearch de Opin
     */
    public function actionPrivadas()
    {
        $model = new OpinSearch(['fk_user'=>Yii::$app->user->id,'publica'=>0]);

        return $model->search(Yii::$app->request->queryParams);
    }

    /**
     * Método para ver una opin privada por el token
     * @author Rodrigo Boet
     * @date 06/11/2016
     * @return Opin objeto
     */
    public function actionGetPrivate($token)
    {
        $model = OpinPrivada::find()->where(['token'=>$token])->one();
        if($model)
        {
            return ['validacion'=>'ok', 'mensaje'=>'Opin retrived',
            'opin'=>$model->fkOpin];
        }
        else
        {
            return ['validacion'=>'error','mensaje'=>'Cannot find Opin'];
        }
    }
}