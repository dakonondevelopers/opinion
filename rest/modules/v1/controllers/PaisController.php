<?php

namespace app\rest\modules\v1\controllers; 

use Yii;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;
use app\models\PaisSearch;

class PaisController extends ActiveController
{

	public $modelClass = 'app\models\Pais';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => ['class' => QueryParamAuth::className()],
        ];
    }
    
	/**
	 * Función donde se definen las acciones estándar del modelo
	 */
	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete", "create" and "view" actions
		unset($actions['delete'], $actions['create']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

		return $actions;
	}

    /**
     * Método para filtrar en el servicio del pais
     */
    public function prepareDataProvider()
    {
        $model = new PaisSearch();

        return $model->search(Yii::$app->request->queryParams);
    }
}