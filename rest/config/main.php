<?php

use yii\web\JsonParse;

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/../../config/params.php')
);



$db     = require(__DIR__ . '/../../config/db.php');

return [
    'id' => 'basic',
    'basePath' => dirname(__DIR__).'/..',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'app\rest\modules\v1\Module',
            'controllerNamespace' => 'app\rest\modules\v1\controllers' 
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
			'enableCookieValidation' => false,
			'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],      
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => ['user','v1/site',
                    'v1/perfil','v1/user','v1/tematica','v1/opin','v1/opin-respuesta',
                    'vi/idiomas','v1/pais']
                ],
                'OPTIONS v1/user/login' => 'v1/user/login',
                'POST v1/user/login' => 'v1/user/login',
                'OPTIONS v1/user/reset-password' => 'v1/user/reset-password',
                'POST v1/user/reset-password' => 'v1/user/reset-password',
                'GET v1/opin/privadas' => 'v1/opin/privadas',
                'GET v1/opin/get-private' => 'v1/opin/get-private',
                'OPTIONS v1/opin-respuesta/participar' => 'v1/opin-respuesta/participar',
                'POST v1/opin-respuesta/participar' => 'v1/opin-respuesta/participar',
                'GET v1/idiomas' => 'v1/idiomas',
                'GET v1/site/upload-picture' => 'v1/site/upload-picture',
            ],
        ],
        'authManager' => [
              'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\PhpManager'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@webroot/../mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'pruebasdakonon5@gmail.com',
                'password' => '123456dakonon',
                'port' => '465',
                'encryption' => 'ssl',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
