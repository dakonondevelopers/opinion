<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OpinionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/opinion.css',
        'css/font-awesome/css/font-awesome.min.css',
        'css/flags/flag-icon.min.css',
        'css/iCheck/all.css',
    ];
    public $js = [
        'js/AdminLTE/app.js',
        'js/iCheck/icheck.min.js',
        'js/dynamic-form/dynamicform.js',
        'js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
