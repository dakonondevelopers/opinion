<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idiomas".
 *
 * @property integer $pk_idioma
 * @property string $idioma
 * @property string $nombre
 *
 * @property Pais[] $pais
 */
class Idiomas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'idiomas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idioma', 'nombre'], 'required'],
            [['idioma'], 'string', 'max' => 2],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_idioma' => 'Pk Idioma',
            'idioma' => 'Idioma',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasMany(Pais::className(), ['idioma' => 'pk_idioma']);
    }
}
