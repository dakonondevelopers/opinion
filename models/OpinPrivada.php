<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin_privada".
 *
 * @property integer $id_opin_privada
 * @property integer $fk_opin
 * @property string $token
 *
 * @property Opin $fkOpin
 */
class OpinPrivada extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin_privada';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_opin', 'token'], 'required'],
            [['fk_opin'], 'integer'],
            [['token'], 'string'],
            [['fk_opin'], 'unique'],
            [['token'], 'unique'],
            [['fk_opin'], 'exist', 'skipOnError' => true, 'targetClass' => Opin::className(), 'targetAttribute' => ['fk_opin' => 'id_opin']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_opin_privada' => 'Id Opin Privada',
            'fk_opin' => 'Fk Opin',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpin()
    {
        return $this->hasOne(Opin::className(), ['id_opin' => 'fk_opin']);
    }
}
