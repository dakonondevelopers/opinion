<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin_stat".
 *
 * @property integer $fk_user
 * @property integer $fk_opin
 *
 * @property Opin $fkOpin
 * @property User $fkUser
 */
class OpinStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin_stat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user', 'fk_opin'], 'required'],
            [['fk_user', 'fk_opin'], 'integer'],
            [['fk_opin'], 'exist', 'skipOnError' => true, 'targetClass' => Opin::className(), 'targetAttribute' => ['fk_opin' => 'id_opin']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fk_user' => 'Fk User',
            'fk_opin' => 'Fk Opin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpin()
    {
        return $this->hasOne(Opin::className(), ['id_opin' => 'fk_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_user']);
    }
}
