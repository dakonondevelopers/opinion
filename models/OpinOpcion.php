<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin_opcion".
 *
 * @property integer $id_opcion
 * @property integer $fk_opin
 * @property string $texto_opcion
 *
 * @property Opin $fkOpin
 * @property OpinRespuesta[] $opinRespuestas
 */
class OpinOpcion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin_opcion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_opin', 'texto_opcion'], 'required'],
            [['fk_opin'], 'integer'],
            [['texto_opcion'], 'string', 'max' => 255],
            [['fk_opin'], 'exist', 'skipOnError' => true, 'targetClass' => Opin::className(), 'targetAttribute' => ['fk_opin' => 'id_opin']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_opcion' => 'Id Opcion',
            'fk_opin' => 'Fk Opin',
            'texto_opcion' => 'Texto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpin()
    {
        return $this->hasOne(Opin::className(), ['id_opin' => 'fk_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinRespuestas()
    {
        return $this->hasMany(OpinRespuesta::className(), ['fk_opcion' => 'id_opcion']);
    }
}
