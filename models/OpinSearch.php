<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Opin;

/**
 * OpinSearch represents the model behind the search form about `app\models\Opin`.
 */
class OpinSearch extends Opin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_opin', 'fk_user', 'seleccion', 'fk_idioma', 'relevancia', 'activo', 'publica'], 'integer'],
            [['titulo', 'opin', 'fecha_creacion', 'fecha_fin', 'opin_foto'], 'safe'],
        ];
    }

    /**
     * Funcion que quita el nombre del form para hacer la busqueda
     */
    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Opin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_opin' => $this->id_opin,
            'fk_user' => $this->fk_user,
            'fk_idioma' => $this->fk_idioma,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_fin' => $this->fecha_fin,
            'relevancia' => $this->relevancia,
            'activo' => $this->activo,
            'publica' => $this->publica,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'opin', $this->opin])
            ->andFilterWhere(['like', 'opin_foto', $this->opin_foto]);

        return $dataProvider;
    }
}
