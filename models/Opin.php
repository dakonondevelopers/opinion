<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin".
 *
 * @property integer $id_opin
 * @property integer $fk_user
 * @property integer $seleccion
 * @property integer $fk_idioma
 * @property string $titulo
 * @property string $opin
 * @property string $fecha_creacion
 * @property string $fecha_fin
 * @property integer $relevancia
 * @property string $opin_foto
 * @property integer $activo
 * @property integer $publica
 *
 * @property Idiomas $fkIdioma
 * @property User $fkUser
 * @property OpinOpcion[] $opinOpcions
 * @property OpinPrivada $opinPrivada
 * @property OpinStat[] $opinStats
 * @property User[] $fkUsers
 * @property OpinTematica[] $opinTematicas
 * @property Tematica[] $fkTematicas
 */
class Opin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user', 'seleccion', 'fk_idioma', 'titulo', 'opin', 'fecha_creacion', 'activo', 'publica'], 'required'],
            [['fk_user', 'seleccion', 'fk_idioma', 'relevancia', 'activo', 'publica'], 'integer'],
            [['fecha_creacion', 'fecha_fin'], 'safe'],
            [['fecha_fin'],'date','format'=>'php:Y-m-d H:i:s'],
            [['titulo', 'opin_foto'], 'string', 'max' => 128],
            [['opin'], 'string', 'max' => 30],
            [['opin'], 'unique'],
            [['fk_idioma'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['fk_idioma' => 'pk_idioma']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_opin' => 'Id Opin',
            'fk_user' => 'Fk User',
            'seleccion' => 'Seleccion',
            'fk_idioma' => 'Fk Idioma',
            'fkIdioma.nombre'=>'Idioma',
            'titulo' => 'Pregunta',
            'opin' => 'Opin',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_fin' => 'Fecha Fin',
            'relevancia' => 'Relevancia',
            'opin_foto' => 'Opin Foto',
            'activo' => 'Activo',
            'publica' => 'Publica',
        ];
    }

    /**
     * @return campos extras
     */
    public function extraFields()
    {
        return [
            'opciones'=>'opinOpcions',
            'privada'=>'opinPrivada'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['pk_idioma' => 'fk_idioma']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinOpcions()
    {
        return $this->hasMany(OpinOpcion::className(), ['fk_opin' => 'id_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinPrivada()
    {
        return $this->hasOne(OpinPrivada::className(), ['fk_opin' => 'id_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinStats()
    {
        return $this->hasMany(OpinStat::className(), ['fk_opin' => 'id_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'fk_user'])->viaTable('opin_stat', ['fk_opin' => 'id_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinTematicas()
    {
        return $this->hasMany(OpinTematica::className(), ['fk_opin' => 'id_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTematicas()
    {
        return $this->hasMany(Tematica::className(), ['id_tematica' => 'fk_tematica'])->viaTable('opin_tematica', ['fk_opin' => 'id_opin']);
    }
}
