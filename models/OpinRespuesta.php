<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin_respuesta".
 *
 * @property integer $id_repuesta
 * @property integer $fk_user
 * @property integer $fk_opcion
 * @property string $fecha_respuesta
 *
 * @property OpinOpcion $fkOpcion
 * @property User $fkUser
 */
class OpinRespuesta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin_respuesta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user', 'fk_opcion', 'fecha_respuesta'], 'required'],
            [['fk_user', 'fk_opcion'], 'integer'],
            [['fecha_respuesta'], 'safe'],
            [['fk_opcion'], 'exist', 'skipOnError' => true, 'targetClass' => OpinOpcion::className(), 'targetAttribute' => ['fk_opcion' => 'id_opcion']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_repuesta' => 'Id Repuesta',
            'fk_user' => 'Fk User',
            'fk_opcion' => 'Fk Opcion',
            'fecha_respuesta' => 'Fecha Respuesta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpcion()
    {
        return $this->hasOne(OpinOpcion::className(), ['id_opcion' => 'fk_opcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_user']);
    }
}
