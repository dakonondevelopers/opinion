<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pais;

/**
 * PaisSearch represents the model behind the search form about `app\models\Pais`.
 */
class PaisSearch extends Pais
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pais', 'idioma'], 'integer'],
            [['nombre_pais', 'iso','idioma0.nombre'], 'safe'],
        ];
    }

    /**
     * Funcion que quita el nombre del form para hacer la busqueda
     */
    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributes()
    {
    // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['idioma0.nombre']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pais::find();

        $query->joinWith(['idioma0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['idioma0.nombre'] = [
            'asc' => ['nombre' => SORT_ASC],
            'desc' => ['nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pais' => $this->id_pais,
            'idioma' => $this->idioma,
        ]);

        $query->andFilterWhere(['like', 'nombre_pais', $this->nombre_pais])
            ->andFilterWhere(['like', 'iso', $this->iso])
            ->andFilterWhere(['idiomas.nombre' => $this->getAttribute('idioma0.nombre')]);

        return $dataProvider;
    }
}
