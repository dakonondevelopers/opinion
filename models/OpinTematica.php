<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opin_tematica".
 *
 * @property integer $fk_tematica
 * @property integer $fk_opin
 *
 * @property Opin $fkOpin
 * @property Tematica $fkTematica
 */
class OpinTematica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opin_tematica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_tematica', 'fk_opin'], 'required'],
            [['fk_tematica', 'fk_opin'], 'integer'],
            [['fk_opin'], 'exist', 'skipOnError' => true, 'targetClass' => Opin::className(), 'targetAttribute' => ['fk_opin' => 'id_opin']],
            [['fk_tematica'], 'exist', 'skipOnError' => true, 'targetClass' => Tematica::className(), 'targetAttribute' => ['fk_tematica' => 'id_tematica']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fk_tematica' => 'Fk Tematica',
            'fk_opin' => 'Fk Opin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpin()
    {
        return $this->hasOne(Opin::className(), ['id_opin' => 'fk_opin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkTematica()
    {
        return $this->hasOne(Tematica::className(), ['id_tematica' => 'fk_tematica']);
    }
}
