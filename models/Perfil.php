<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perfil".
 *
 * @property integer $id_perfil
 * @property string $telefono
 * @property string $genero
 * @property integer $edad
 * @property string $fecha_nacimiento
 * @property integer $fk_user
 * @property integer $fk_pais
 *
 * @property Pais $fkPais
 * @property User $fkUser
 */
class Perfil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perfil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['telefono', 'genero', 'edad', 'fecha_nacimiento', 'fk_user', 'fk_pais'], 'required'],
            [['edad', 'fk_user', 'fk_pais'], 'integer'],
            [['fecha_nacimiento'], 'safe'],
            [['telefono'], 'string', 'max' => 25],
            [['genero'], 'string', 'max' => 1],
            [['fk_user'], 'unique'],
            [['fk_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Pais::className(), 'targetAttribute' => ['fk_pais' => 'id_pais']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_perfil' => 'Id Perfil',
            'telefono' => 'Telefono',
            'genero' => 'Genero',
            'edad' => 'Edad',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'fk_user' => 'Fk User',
            'fk_pais' => 'Fk Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPais()
    {
        return $this->hasOne(Pais::className(), ['id_pais' => 'fk_pais']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_user']);
    }
}
