<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pais".
 *
 * @property integer $id_pais
 * @property string $nombre_pais
 * @property string $iso
 * @property integer $idioma
 *
 * @property Opin[] $opins
 * @property Idiomas $idioma0
 * @property Perfil[] $perfils
 */
class Pais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_pais', 'iso', 'idioma'], 'required'],
            [['idioma'], 'integer'],
            [['nombre_pais'], 'string', 'max' => 128],
            [['iso'], 'string', 'max' => 2],
            [['idioma'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['idioma' => 'pk_idioma']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pais' => 'Id Pais',
            'nombre_pais' => 'Nombre Pais',
            'iso' => 'Iso',
            'idioma' => 'Idioma',
            'idioma0.nombre' => 'Idioma',
        ];
    }

    /**
     * @return campos extras
     */
    public function extraFields()
    {
        return ['idioma_nombre'=>'idioma0'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpins()
    {
        return $this->hasMany(Opin::className(), ['fk_pais' => 'id_pais']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma0()
    {
        return $this->hasOne(Idiomas::className(), ['pk_idioma' => 'idioma']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfils()
    {
        return $this->hasMany(Perfil::className(), ['fk_pais' => 'id_pais']);
    }
}
