<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tematica".
 *
 * @property integer $id_tematica
 * @property string $titulo
 * @property integer $activa
 *
 * @property OpinTematica[] $opinTematicas
 * @property Opin[] $fkOpins
 */
class Tematica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tematica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'activa'], 'required'],
            [['activa'], 'integer'],
            [['titulo'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tematica' => 'Id Tematica',
            'titulo' => 'Titulo',
            'activa' => 'Activa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpinTematicas()
    {
        return $this->hasMany(OpinTematica::className(), ['fk_tematica' => 'id_tematica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkOpins()
    {
        return $this->hasMany(Opin::className(), ['id_opin' => 'fk_opin'])->viaTable('opin_tematica', ['fk_tematica' => 'id_tematica']);
    }
}
