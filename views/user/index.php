<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Exportar Correos', ['getmails'], ['class' => 'btn btn-info']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            ['attribute'=>'fkAuthAssignment.item_name','filter'=>array('Admin'=>'Admin','Usuario'=>'Usuario')],
            [        
                'attribute' => 'status',
                'filter'=>array("0"=>"Inactivo","10"=>"Activo"),
                'value' => function ($model) 
                {
                    return $model->status == 10 ? 'Activo' : 'Inactivo';
                },
            ],
            [        
                'attribute' => 'created_at',
                'value' => function ($model) 
                {
                    return date('Y-m-d',$model->created_at);
                },
            ],
            [        
                'attribute' => 'updated_at',
                'value' => function ($model) 
                {
                    return date('Y-m-d',$model->updated_at);
                },
            ],
            'fkPerfil.fkPais.nombre_pais',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}{activate}',
                'buttons'=>[
                    'activate' => function($url,$model)
                    {
                        return $model->status == 10 ? Html::a(
                        '<i class="icon-unlock"></i>',
                        ['desactivate','id'=>$model->id]
                        ):Html::a(
                        '<i class="icon-lock"></i>',
                        ['activate','id'=>$model->id])
                        ;
                    },
                ],
            ],
        ],
    ]); ?>
</div>
