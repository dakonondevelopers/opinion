<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view content">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            ['attribute' => 'status','value' => $model->status == 10 ? 'Activo':'Inactivo'],
            ['attribute' => 'created_at','value' => date('Y-m-d',$model->created_at)],
            ['attribute' => 'updated_at','value' => date('Y-m-d',$model->updated_at)],
            'fkPerfil.telefono',
            ['attribute' => 'fkPerfil.genero','value' => $model->fkPerfil->genero == 'M' ? 'Masculino':'Femenino'],
            'fkPerfil.edad',
            'fkPerfil.fecha_nacimiento',
            'fkPerfil.fkPais.nombre_pais',
        ],
    ]) ?>

</div>
