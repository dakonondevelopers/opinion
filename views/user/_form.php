<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => true,]); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

	<?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <?= $form->field($model, 'genero')->radioList(['M' => 'Masculino', 'F' => 'Femenino'])->label('Genero') ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'fecha_nacimiento')->widget(DatePicker::classname(), [
        'language' => 'es',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'pais')->dropDownList(ArrayHelper::map($pais, 'id_pais', 'nombre_pais'), 
     ['prompt'=>'-Escoge un País-'])->label('País') ?>

    <?= Html::label('Roles', 'roles', ['class' => 'label_roles']) ?>
    <?= Html::radioList('roles', 'USUARIO', ['USUARIO' => 'Usuario', 'ADMIN' => 'Admin'],['class'=>'form-group']) ?>

    <div class="form-group">
        <?= Html::submitButton('Create' , ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
