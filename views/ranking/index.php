<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ranking';
$this->params['breadcrumbs'][] = $this->title;
?>
<ul id="tabTable" class="nav nav-tabs">
	<li class="active">
		<a href="#general" data-toggle="tab">Ranking General</a>
	</li>
	<li>
		<a href="#usuarios" data-toggle="tab">Top 10 Usuarios</a>
	</li>
	<li>
		<a href="#paises" data-toggle="tab">Top 10 Paises</a>
	</li>
	<li>
		<a href="#opin" data-toggle="tab">Top 10 Opin</a>
	</li>
</ul>

<div id="tabTableContent" class="tab-content">
	<!- Contenido General de Rankings ->
	<div class="tab-pane fade in active" id="general">
		<div class="ranking-index content">
			<div class="table-responsive">
				<table class="table table-striped">
					<caption class="text-center"><i class=" icon-trophy gold"></i>Ranking General</caption>
					<tbody>
						<tr>
							<th>Nº Total de Usuarios Activos</th>
							<td><?= $activo ?></td>
						</tr>
						<tr>
							<th>Nº Total de Usuarios Pendientes</th>
							<td><?= $inactivo ?></td>
						</tr>
						<tr>
							<th>Nº Total de Opins</th>
							<td><?= $opin ?></td>
						</tr>
						<tr>
							<th>Nº Total de Votaciones</th>
							<td><?= $votos ?></td>
						</tr>
						<tr>
							<th>Nº Votos del Opin más votado</th>
							<td><?= $opin_mas_votada ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!- Contenido Top 10 Usuarios ->
	<div class="tab-pane fade" id="usuarios">
		<div class="ranking-index content">
			<div class="table-responsive">
				<table class="table table-striped">
					<caption class="text-center"><i class=" icon-trophy gold"></i>Top 10 Usuarios con más votos en Opin</caption>
					<thead>
						<tr>
							<th>Nombre de Usuario</th>
							<th>Votos</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach ($top_10_usuario as $key => $value) {
							echo '<tr>';
							echo '<td>'.$value['username'].'</td>';
							echo '<td>'.$value['votos'].'</td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!- Contenido Top 10 Paises ->
	<div class="tab-pane fade" id="paises">
		<div class="ranking-index content">
			<div class="table-responsive">
				<table class="table table-striped">
					<caption class="text-center"><i class=" icon-trophy gold"></i>Top 10 Paises con más usuarios activos</caption>
					<thead>
						<tr>
							<th>Nombre del País</th>
							<th>Usuario Activos</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach ($top_10_pais as $key => $value) {
							echo '<tr>';
							echo '<td>'.$value['nombre_pais'].'</td>';
							echo '<td>'.$value['cantidad'].'</td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!- Contenido Top 10 Opin ->
	<div class="tab-pane fade" id="opin">
		<div class="ranking-index content">
			<div class="table-responsive">
				<table class="table table-striped">
					<caption class="text-center"><i class=" icon-trophy gold"></i>Top 10 Opin con más respuestas</caption>
					<thead>
						<tr>
							<th>Opin</th>
							<th>Respuestas</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach ($top_10_opin as $key => $value) {
							echo '<tr>';
							echo '<td>'.$value['titulo'].'</td>';
							echo '<td>'.$value['respuestas'].'</td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>