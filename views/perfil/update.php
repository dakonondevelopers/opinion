<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Perfil */

$this->title = 'Actualizar Perfil: ';
$this->params['breadcrumbs'][] = 'Perfil';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perfil-update content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pais' => $pais,
    ]) ?>

</div>
