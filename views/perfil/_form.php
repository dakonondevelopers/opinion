<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Perfil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perfil-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'genero')->radioList(['M' => 'Masculino', 'F' => 'Femenino'])->label('Genero') ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'fecha_nacimiento')->widget(DatePicker::classname(), [
        'language' => 'es',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'fk_pais')->dropDownList(ArrayHelper::map($pais, 'id_pais', 'nombre_pais'), 
     ['prompt'=>'-Escoge un País-'])->label('País') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
