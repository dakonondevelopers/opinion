<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Opin */

$this->title = 'Actualizar Encuestas: ' . $opin->id_opin;
$this->params['breadcrumbs'][] = ['label' => 'Encuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $opin->id_opin, 'url' => ['view', 'id' => $opin->id_opin]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="opin-update content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsOpcion' => $modelsOpcion,
        'tematica' => $tematica,
        'new' => False,
    ]) ?>

</div>
