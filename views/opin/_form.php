<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Opin */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="opin-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opin')->textInput(['maxlength' => true,'id'=>'opin']) ?>

    <?= $form->field($model, 'fecha_fin')->widget(DateTimePicker::classname(), [
        'language' => 'es',
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'y-MM-dd H:m:s',
            'todayHighlight' => true
        ]
    ]) ?>
    
    <div class="form-group">
        <label class="col-sm-3">Imagen de la Encuesta:</label>
        <div class="col-sm-9">
            <label for="files" data-role="button" class="img-responsive cambiar_img" data-inline="true" data-mini="true" data-corners="false">
                <p class="text-img" style="margin-top:70px;">subir imágen<br/>
                </p>
            </label>
                <?= $form->field($model, "opin_foto")->fileInput(
                    ['class'=>'','id'=>'files','style'=>'opacity:0;']
                )->label(false); ?> 
        </div>
    </div>

    <?= $form->field($model, 'tematica[]')->checkboxList(ArrayHelper::map($tematica, 'id_tematica', 'titulo'), 
     ['multiple'=>True])->label('Temática') ?>

     <?= $form->field($model, 'seleccion')->radioList([0 => 'Selección Simple',1 => 'Selección Múltiple'])->label('Tipo de Opin') ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-th-list"></i> Respuestas</h4></div>
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 15, // the maximum times, an element can be cloned (default 999)
                'min' => 3, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsOpcion[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'texto_opcion',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsOpcion as $i => $modelOpcion): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Opcion</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelOpcion->isNewRecord) {
                                echo Html::activeHiddenInput($modelOpcion, "[{$i}]id_opcion");
                            }
                        ?>
                        <?= $form->field($modelOpcion, "[{$i}]texto_opcion")->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <?= $form->field($model, 'activo')->checkbox() ?>

    <?= $form->field($model, 'publica')->radioList([0 => 'Privada', 1 => 'Pública'])->label('Tipo de Encuesta') ?>


    <div class="form-group">
        <?= Html::submitButton($new ? 'Crear' : 'Actualizar', ['class' => $new ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
