<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Opin */

$this->title = 'Crear Encuestas';
$this->params['breadcrumbs'][] = ['label' => 'Encuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->activo = 1;
$model->publica = 1;
$model->opin = "%";
?>
<div class="opin-create content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsOpcion' => $modelsOpcion,
        'tematica' => $tematica,
        'new' => True,
    ]) ?>

</div>
