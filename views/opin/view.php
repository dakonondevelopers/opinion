<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Opin */

$this->title = $model->id_opin;
$this->params['breadcrumbs'][] = ['label' => 'Encuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opin-view content">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_opin], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_opin], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Deseas borrar esta encuesta?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_opin',
            'fkUser.username',
            'seleccion',
            'fkIdioma.nombre',
            'titulo',
            'opin',
            'fecha_creacion',
            'fecha_fin',
            'relevancia',
            ['attribute'=>'opin_foto','value'=> $model->opin_foto!='' ? Yii::getAlias('@web').$model->opin_foto:Yii::getAlias('@web').'/img/not_upload.png',
            'format' => ['image',['width'=>'200','height'=>'200','class'=>'img-rounded']],
            ],
            ['attribute'=>'activo','value'=>$model->activo==1 ? 'Si':'No'],
            ['attribute'=>'publica','value'=>$model->publica==1 ? 'Si':'No']
        ],
    ]) ?>

    <?php
        if(!$model->publica)
        {
            echo '<h4>Para sala privada</h4>';
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    ['attribute'=>'token','value'=> $model->opinPrivada->token],
                ],
            ]);
        }
    ?>

    <?php
    if($model->fkTematicas)
    {
        echo '<h3>Temáticas</h3>';
        
        foreach ($model->fkTematicas as $value) {
            echo DetailView::widget([
            'model' => $value,
            'attributes' => [
                'titulo',
            ],
            ]);
        }
    }
    ?>

    <h3>Opciones</h3>

    <?php foreach ($opcion as $value) {
        echo DetailView::widget([
        'model' => $value,
        'attributes' => [
            'texto_opcion',
        ],
        ]);
    }
    ?>

</div>
