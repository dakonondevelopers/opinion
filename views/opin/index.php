<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OpinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Encuestas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opin-index content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Encuesta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_opin',
            //'fk_user',
            //'fk_seleccion',
            'titulo',
            'opin',
            'fecha_creacion',
            'fecha_fin',
            // 'relevancia',
            // 'opin_foto',
            // 'activo',
            // 'publica',
            [        
                'attribute' => 'activo',
                'filter'=>array("0"=>"Inactiva","1"=>"Activa"),
                'value' => function ($model) 
                {
                    return $model->activo == True ? 'Activa' : 'Inactiva';
                },
            ],
            [        
                'attribute' => 'publica',
                'filter'=>array("0"=>"Privada","1"=>"Pública"),
                'value' => function ($model) 
                {
                    return $model->publica == True ? 'Pública' : 'Privada';
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}{activate}',
                'buttons'=>[
                    'activate' => function($url,$model)
                    {
                        return $model->activo == 1 ? Html::a(
                        '<i class="icon-unlock"></i>',
                        ['desactivate','id'=>$model->id_opin]
                        ):Html::a(
                        '<i class="icon-lock"></i>',
                        ['activate','id'=>$model->id_opin])
                        ;
                    },
                ],
            ],
        ],
    ]); ?>
</div>
