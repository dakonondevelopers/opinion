<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OpinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_opin') ?>

    <?= $form->field($model, 'fk_user') ?>

    <?= $form->field($model, 'fk_tematica') ?>

    <?= $form->field($model, 'fk_seleccion') ?>

    <?= $form->field($model, 'fk_pais') ?>

    <?php // echo $form->field($model, 'titulo') ?>

    <?php // echo $form->field($model, 'opin') ?>

    <?php // echo $form->field($model, 'texto_opin') ?>

    <?php // echo $form->field($model, 'fecha_creacion') ?>

    <?php // echo $form->field($model, 'fecha_inicio') ?>

    <?php // echo $form->field($model, 'fecha_fin') ?>

    <?php // echo $form->field($model, 'relevancia') ?>

    <?php // echo $form->field($model, 'opin_foto') ?>

    <?php // echo $form->field($model, 'activo') ?>

    <?php // echo $form->field($model, 'publica') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
