<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Pais */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pais-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_pais')->textInput(['maxlength' => true, 'disabled'=>$disabled])->label('Nombre del País') ?>

    <?= $form->field($model, 'iso')->textInput(['maxlength' => true, 'disabled'=>$disabled])->label('Iso') ?>

    <?= $form->field($model, 'idioma')->dropDownList(ArrayHelper::map($idiomas, 'pk_idioma', 'nombre'),
     ['prompt'=>'-Escoge el idioma-'])->label('Idioma') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
