<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tematica */

$this->title = $model->id_tematica;
$this->params['breadcrumbs'][] = ['label' => 'Tematicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tematica-view content">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_tematica], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_tematica], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Deseas eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_tematica',
            'titulo',
            'activa',
        ],
    ]) ?>

</div>
