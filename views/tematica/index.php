<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TematicaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tematicas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tematica-index content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Tematica', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tematica',
            'titulo',
            'activa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
