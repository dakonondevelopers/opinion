<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tematica */

$this->title = 'Crear Tematica';
$this->params['breadcrumbs'][] = ['label' => 'Tematicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tematica-create content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
