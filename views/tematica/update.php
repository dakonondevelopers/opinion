<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tematica */

$this->title = 'Actualizar Tematica: ' . $model->id_tematica;
$this->params['breadcrumbs'][] = ['label' => 'Tematicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tematica, 'url' => ['view', 'id' => $model->id_tematica]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tematica-update content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
