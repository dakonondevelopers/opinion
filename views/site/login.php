<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-box" id="login-box">

    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="header bg-morado">
            <img src="<?= Yii::getAlias('@web'); ?>/img/favicon-white.png" width="32" height="32" style="margin-right: 16px">
            opinionapp
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal skin-morado box'],
            'fieldConfig' => [
                'template' => "<div class=\"col-md-12\">{input}</div>\n",
                'labelOptions' => ['class' => 'col-md-3 control-label'],
            ],
        ]); ?>
        <div class="body bg-gray">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Usuario'])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Contraseña'])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-md-offset-1 col-md-3\">{input} {label}</div>\n",
            ]) ?>
        </div>

            <div class="form-group text-center button-form">
                <div class="row">
                    <?= Html::submitButton('Entrar', ['class' => 'btn logo xs', 'name' => 'login-button']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-4"></div>
</div>

