<?php

/* @var $this yii\web\View */

$this->title = 'Opinión';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-2">
            <div class="small-box bg-green">
                <div class="inner row">
                    <div class="col-md-4 text-left">
                    <h3>
                        <?php
                            print($opin_pub);
                        ?>
                    </h3>
                    <p>
                        Libres
                    </p>
                    </div>

                    <div class="col-md-4 text-center">
                        <h4>Encuestas</h4>
                    </div>

                    <div class="col-md-4 text-right">
                        <h3>
                        <?php
                            print($opin_priv);
                        ?>
                    </h3>
                    <p>
                        Cerradas
                    </p>
                    </div>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
                <a href="<?= Yii::getAlias('@web') ?>/opin" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <?php
                            print($tematica);
                        ?>
                    </h3>
                    <p>
                        Temáticas
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-list-alt"></i>
                </div>
                <a href="<?= Yii::getAlias('@web') ?>/tematica" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3>
                        <?php
                            print($paises);
                        ?>
                    </h3>
                    <p>
                        Paises
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?= Yii::getAlias('@web') ?>/pais" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <?php
                            print($users);
                        ?>
                    </h3>
                    <p>
                        Usuarios (+<?php echo $admins;?> administradores)
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?= Yii::getAlias('@web') ?>/user" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="small-box bg-purple" onclick="location.href='/notificaciones'">
                <div class="inner">
                    <h3>&nbsp;</h3>
                    <p>Notificaciones</p>
                </div>
                <div class="icon">
                    <i class="fa fa-bullhorn"></i>
                </div>
                <a href="/notificaciones" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>

    </div>
</div>
