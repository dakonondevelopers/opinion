<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\OpinionAsset;
use app\widgets\Alert;

OpinionAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-morado">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php //include('cabecera.php') ?>
    <?php
    /*NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();*/
    ?>
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#asd">
                <span class="sr-only">Menú de navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <a class="navbar-brand logo" href="<?= Yii::getAlias("@web") ?>">
                <div >
                <img src="<?= Yii::getAlias('@web') ?>/img/favicon-white.png" width="32" height="32">
                opinionapp
                </div>
            </a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user"></i>
                        <span><?php ?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="skin-morado user-header">
                            <p>
                                <?php  ?>
                                <br><br>
                                <small>Administrador desde <?php //echo Fecha::timestamp2fecha($_SESSION['fechaRegistro'])?></small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Yii::getAlias('@web') ?>/perfil/update?id=<?= Yii::$app->user->id?>" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= Yii::getAlias('@web') ?>/site/logout" class="btn btn-default btn-flat" data-method="post">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

        <div class="wrapper row-offcanvas row-offcanvas-left" id="asd">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <ul class="sidebar-menu">                        
                        <?php 
                        $menu = 'Inicio';            
                            // 1. PANTALLA DE INICIO                        
                            if ($menu == 'Inicio')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="'.Yii::getAlias("@web").'"><i class="icon-home"></i>Inicio</a>');
                            print("</li>\n");


                            // 2. ZONA DE ENCUESTAS
                            // 2.1. LIBRES
                            if ($menu == 'Encuestas0')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="'.Yii::getAlias("@web").'/opin"><i class="icon-check"></i>Encuestas</a>');                                    
                            print("</li>\n");
                            

                            // 3. ZONA DE TEMÁTICAS                               
                            if ($menu == 'Tematicas')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="'.Yii::getAlias("@web").'/tematica" ><i class="icon-list-alt"></i>Temáticas</a>');
                            print("</li>\n");

                            // 4. ZONA DE TEMÁTICAS                               
                            if ($menu == 'Paises')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="'.Yii::getAlias("@web").'/pais" ><i class="icon-globe"></i>Paises</a>');
                            print("</li>\n");


                            // 5. ZONA DE USUARIOS
                            if ($menu == 'Usuarios' || $menu == 'Admins' || $menu == 'Basicos')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }

                            print('<a href="'.Yii::getAlias("@web").'/user"><i class="icon-group"></i>Usuarios<i class="fa fa-angle-left pull-right"></i></a>' . "\n");
                            print("</li>\n");

                            // 6. ZONA DE RANKING
                            if ($menu == 'Ranking')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="'.Yii::getAlias("@web").'/ranking"><i class="icon-trophy"></i>Ranking</a>');
                            print("</li>\n");
                            
                          
                            // 7. ZONA DE NOTIFICACIONES GENERALES
                            if ($menu == 'Notificaciones')
                            {
                                print('<li class="active">' . "\n");
                            }
                            else
                            {
                                print('<li>' . "\n");
                            }
                            print('<a href="notificaciones"><i class="icon-bullhorn"></i>Notificaciones</a>');
                            print("</li>\n");
                        ?>
                    </ul>
                </section>
            </aside>

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <i style="margin-right: 10px"></i>
            </h1>
            <ol>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            </ol>
        </section>
        <div class="row">
            <div class="content">
                <div class="row">
                    <?= Alert::widget() ?>
                </div>
                <?= $content ?>
            </div>
        </div>
    </aside>


</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
