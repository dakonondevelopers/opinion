<?php

use yii\db\Migration;

class m161022_225056_opin_opcion extends Migration
{
    public function up()
    {
         $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_opcion}}',[
            'id_opcion' => $this->primaryKey(),
            'fk_opin' => $this->integer()->notNull(),
            'texto_opcion' => $this->string(255)->notNull(),
            ], $tableOptions);

        $this->createIndex('i-fk_opin','opin_opcion','fk_opin');
        $this->addForeignKey('opin-fk_opin','opin_opcion','fk_opin','opin','id_opin','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin_opcion}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
