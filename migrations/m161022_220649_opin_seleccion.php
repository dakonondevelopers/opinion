<?php

use yii\db\Migration;

class m161022_220649_opin_seleccion extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_seleccion}}',[
            'id_seleccion' => $this->primaryKey(),
            'opcion' => $this->string(128)->notNull(),
            'activa' => $this->boolean()->notNull()
            ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%opin_seleccion}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
