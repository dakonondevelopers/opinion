<?php

use yii\db\Migration;

class m161207_005815_opin_tematica extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_tematica}}',[
            'fk_tematica' => $this->integer()->notNull(),
            'fk_opin' => $this->integer()->notNull(),
            'PRIMARY KEY (fk_tematica, fk_opin)',
            ], $tableOptions);

        $this->createIndex('i-fk_tematica','opin_tematica','fk_tematica');
        $this->createIndex('i-fk_opin','opin_tematica','fk_opin');
        $this->addForeignKey('opin_tematica-fk_tematica','opin_tematica','fk_tematica','tematica','id_tematica','CASCADE','CASCADE');
        $this->addForeignKey('opin_tematica-fk_opin','opin_tematica','fk_opin','opin','id_opin','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin_tematica}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
