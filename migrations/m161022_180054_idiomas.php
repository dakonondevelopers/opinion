<?php

use yii\db\Migration;

class m161022_180054_idiomas extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%idiomas}}',[
            'pk_idioma' => $this->primaryKey(),
            'idioma' => $this->string(2)->notNull(),
            'nombre' => $this->string(50)->notNull()
            ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%idiomas}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
