<?php

use yii\db\Migration;

class m161022_225757_opin_respuesta extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_respuesta}}',[
            'id_repuesta' => $this->primaryKey(),
            'fk_user' => $this->integer()->notNull(),
            'fk_opcion' => $this->integer()->notNull(),
            'fecha_respuesta' => $this->datetime()->notNull(),
            ], $tableOptions);

        $this->createIndex('i-fk_user','opin_respuesta','fk_user');
        $this->createIndex('i-fk_opcion','opin_respuesta','fk_opcion');
        $this->addForeignKey('user_id-fk_user','opin_respuesta','fk_user','user','id','CASCADE','CASCADE');
        $this->addForeignKey('opin_opcion-fk_opcion','opin_respuesta','fk_opcion','opin_opcion','id_opcion','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin_respuesta}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
