<?php

use yii\db\Migration;

class m161030_210834_opin_privada extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_privada}}',[
            'id_opin_privada' => $this->primaryKey(),
            'fk_opin' => $this->integer()->notNull()->unique(),
            'token' => $this->string(15)->notNull()->unique(),
            ], $tableOptions);

        $this->createIndex('i-fk_opin','opin_privada','fk_opin');
        $this->addForeignKey('opin_privada-opin','opin_privada','fk_opin','opin','id_opin','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin_privada}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
