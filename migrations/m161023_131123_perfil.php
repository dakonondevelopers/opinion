<?php

use yii\db\Migration;

class m161023_131123_perfil extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%perfil}}',[
            'id_perfil' => $this->primaryKey(),
            'telefono' => $this->string(25)->notNull(),
            'genero' => $this->string(1)->notNull(),
            'edad' => $this->integer(3)->notNull(),
            'fecha_nacimiento' => $this->date()->notNull(),
            'fk_user' => $this->integer()->notNull(),
            'fk_pais' => $this->integer()->notNull(),
            'UNIQUE (fk_user)'
            ], $tableOptions);

        $this->createIndex('i-fk_user','perfil','fk_user');
        $this->createIndex('i-fk_pais','perfil','fk_pais');
        $this->addForeignKey('perfil_usuario','perfil','fk_user','user','id','CASCADE','CASCADE');
        $this->addForeignKey('perfil_pais','perfil','fk_pais','pais','id_pais','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%perfil}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
