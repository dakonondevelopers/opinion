<?php

use yii\db\Migration;

class m161204_153640_opin_stat extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin_stat}}',[
            'fk_user' => $this->integer()->notNull(),
            'fk_opin' => $this->integer()->notNull(),
            'PRIMARY KEY (fk_user, fk_opin)',
            ], $tableOptions);

        $this->createIndex('i-fk_user','opin_stat','fk_user');
        $this->createIndex('i-fk_opin','opin_stat','fk_opin');
        $this->addForeignKey('user_id_stat-fk_user','opin_stat','fk_user','user','id','CASCADE','CASCADE');
        $this->addForeignKey('opin_opcion_stat-fk_opin','opin_stat','fk_opin','opin','id_opin','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin_stat}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
