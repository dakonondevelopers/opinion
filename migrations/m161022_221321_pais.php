<?php

use yii\db\Migration;

class m161022_221321_pais extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pais}}',[
            'id_pais' => $this->primaryKey(),
            'nombre_pais' => $this->string(128)->notNull(),
            'iso' => $this->string(2)->notNull(),
            'idioma' => $this->integer()->notNull()
            ], $tableOptions);

        $this->createIndex('i-fk_idioma','pais','idioma');
        $this->addForeignKey('idioma_pais','pais','idioma','idiomas','pk_idioma','CASCADE','CASCADE');

    }

    public function down()
    {
        $this->dropTable('{{%pais}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
