<?php

use yii\db\Migration;

class m161022_221552_opin extends Migration
{
    public function up()
    {
        $tableOptions =null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%opin}}',[
            'id_opin' => $this->primaryKey(),
            'fk_user' => $this->integer()->notNull(),
            'seleccion' => $this->integer()->notNull(),
            'fk_idioma' => $this->integer()->notNull(),
            'titulo' => $this->string(128)->notNull(),
            'opin' => $this->string(30)->notNull()->unique(),
            'fecha_creacion' => $this->date()->notNull(),
            'fecha_fin' => $this->datetime(),
            'relevancia' => $this->integer(1)->defaultValue(0),
            'opin_foto' => $this->string(128),
            'activo' => $this->boolean()->notNull(),
            'publica' => $this->boolean()->notNull(),
            ], $tableOptions);

        $this->createIndex('i-fk_user','opin','fk_user');
        $this->createIndex('i-fk_idioma','opin','fk_idioma');
        $this->addForeignKey('user-fk_user','opin','fk_user','user','id','CASCADE','CASCADE');
        $this->addForeignKey('idioma-fk_idioma','opin','fk_idioma','idiomas','pk_idioma','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%opin}}');
        echo "Se borro la tabla con exito.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
