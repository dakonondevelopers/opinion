<?php
return [
	'adminEmail' => 'opinion@opinion.com',
	'Title' => 'Opinión',
	'user.passwordResetTokenExpire' => 3600,
];
