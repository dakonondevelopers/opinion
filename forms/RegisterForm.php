<?php
namespace app\forms;

use yii\base\Model;

/**
 * Signup form
 */
class RegisterForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $telefono;
    public $genero;
    public $edad;
    public $fecha_nacimiento;
    public $pais;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],

            [['password','telefono','genero','edad','fecha_nacimiento','pais'], 'required'],
            ['password', 'string', 'min' => 6],
            ['genero', 'string', 'max' => 1],
            [['edad','pais'], 'integer'],
            ['fecha_nacimiento','date','format' => 'php:Y-m-d'],
            ['telefono', 'string', 'min' => 11, 'max' => 25],
            [['edad','pais','fecha_nacimiento'],'safe'],
            [['pais'], 'exist', 'skipOnError' => true, 'targetClass' => '\app\models\Pais', 'targetAttribute' => ['pais' => 'id_pais']],

        ];
    }
}
