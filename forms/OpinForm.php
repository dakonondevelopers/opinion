<?php
namespace app\forms;

use yii\base\Model;
use app\models\Tematica;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 */
class OpinForm extends Model
{
    public $fk_user;
    public $seleccion;
    public $fk_idioma;
    public $titulo;
    public $opin;
    public $fecha_creacion;
    public $fecha_fin;
    public $relevancia;
    public $opin_foto;
    public $activo;
    public $tematica;
    public $publica;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user', 'seleccion', 'fk_idioma', 'titulo', 'opin', 'fecha_creacion', 'activo', 'publica'], 'required'],
            [['fk_user', 'seleccion', 'fk_idioma', 'relevancia', 'activo', 'publica'], 'integer'],
            [['fecha_creacion', 'fecha_fin','tematica'], 'safe'],
            [['fecha_fin'],'date','format'=>'php:Y-m-d H:i:s'],
            [['titulo', 'opin_foto'], 'string', 'max' => 128],
            [['opin'], 'string', 'max' => 30],
            ['opin', 'trim'],
            ['opin','match','pattern'=>'/^\B%[\w_]+$/','message'=>'Opin not in format'],
            ['opin', 'unique', 'targetClass' => '\app\models\Opin'],
            ['tematica','validateTematica'],
            [['fk_idioma'], 'exist', 'skipOnError' => true, 'targetClass' => 'app\models\Idiomas', 'targetAttribute' => ['fk_idioma' => 'pk_idioma']],
            [['fk_user'], 'exist', 'skipOnError' => true, 'targetClass' => 'app\models\User', 'targetAttribute' => ['fk_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_opin' => 'Id Opin',
            'fk_user' => 'Fk User',
            'seleccion' => 'Seleccion',
            'fk_idioma' => 'Fk Idioma',
            'titulo' => 'Pregunta',
            'opin' => 'Opin',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_fin' => 'Fecha Fin',
            'relevancia' => 'Relevancia',
            'opin_foto' => 'Opin Foto',
            'activo' => 'Activo',
            'publica' => 'Publica',
        ];
    }

    /**
     * Función para validar que la tematica pertenezca a lo registrado en BD
     * @author Rodrigo Boet
     * @date 06/12/2016
     */
    public function validateTematica($attribute,$params)
    {
        $tematicas = Tematica::find()->select(['id_tematica'])->where(['activa'=>1])->asArray()->all();
        $keys = array_keys(ArrayHelper::map($tematicas, 'id_tematica', 'id_tematica'));
        $values = array_intersect($this->$attribute, $keys);
        if(count($values)<=0)
        {
            $this->addError($attribute, 'Tematica invalid.');  
        }
    }
}