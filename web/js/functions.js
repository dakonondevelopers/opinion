/**
* Función para cargar la instancia de una imagen cargada por
* un archivo en un contenedor
*/
$("#files").change(function(event) {
	$(".text-img").hide();
    $.each(event.target.files, function(index, file) {
      var reader = new FileReader();
      reader.onload = function(event) {  
        object = {};
        object.filename = file.name;
        object.data = event.target.result;
        base64 = object.data;
        $(".cambiar_img").css("background-size", "100% 100%");
        $(".cambiar_img").css("background-image", "url(" + base64 + ")");
      };  
      reader.readAsDataURL(file);
    });
});


/**
* Función para sólo permitir el formato permitido en la opin
*/
$('#opin').keyup(function(){
  var value = $(this).val();
  $(this).val($(this).val().split(" ").join(""))
  if(value[0]!="%")
  {
    $(this).val('');
  }  
});