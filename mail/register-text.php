<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

//$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$resetLink = "http://localhost/opinion/web/site/confirm?key=".$user->auth_key;
?>
Bienvenido <?= $user->username ?>,

Hemos recibido una solicitud de alta con los siguientes detalles:


Usuario: <?= $user->username ?>
País: <?= $perfil->fkPais->nombre_pais ?>
Género: <?= $perfil->genero ?>
Fecha de nacimiento: <?= $perfil->fecha_nacimiento ?>
Registrado en: <?= date('d-m-Y',$user->created_at)?>

Si es correcto y desea confirmar pulse el siguiente botón:

<?= $resetLink ?>
