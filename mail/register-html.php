<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

//$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$resetLink = "http://localhost/opinion/web/site/confirm?key=".$user->auth_key;
?>
<style type="text/css">
.btn {
    font-weight: 500;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    border: 1px solid transparent;
    -webkit-box-shadow: inset 0px -2px 0px 0px rgba(0, 0, 0, 0.09);
    -moz-box-shadow: inset 0px -2px 0px 0px rgba(0, 0, 0, 0.09);
    box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);
}

.btn.btn-success {
    background-color: #00a65a;
    border-color: #008d4c;
}
</style>
<div class="register">
    <p>Bienvenido <?= Html::encode($user->username) ?>,</p>

    <p>
       Hemos recibido una solicitud de alta con los siguientes detalles:
   </p>
   <ul>
       <li>Usuario: <?= $user->username?></li>
       <li>País: <?= $perfil->fkPais->nombre_pais ?></li>
       <li>Género: <?= $perfil->genero=="M" ? "Masculino":"Femenino" ?></li>
       <li>Fecha de nacimiento: <?= $perfil->fecha_nacimiento ?></li>
       <li>Registrado en: <?= date('d-m-Y',$user->created_at)?></li>
   </ul>
   
   <br>
   
   <p>
       Si es correcto y desea confirmar pulse el siguiente botón:
   </p>

   <p><?= Html::a("Confirmar Cuenta", $resetLink) ?></p>
</div>
