<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

//$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$resetLink = "http://localhost/opinion/web/site/reset-password?token=".$user->password_reset_token;
?>
Hola <?= $user->username ?>,

Siga el enlace de abajo para resetear la contraseña:

<?= $resetLink ?>
