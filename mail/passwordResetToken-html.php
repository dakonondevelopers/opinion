<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

//$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$resetLink = "http://localhost/opinion/web/site/reset-password?token=".$user->password_reset_token;
?>
<div class="password-reset">
    <p>Bienvenido <?= Html::encode($user->username) ?>,</p>

    <p>Siga el siguiente enlace para restablecer la contraseña:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
