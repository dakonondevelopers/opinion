-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-11-2016 a las 15:36:03
-- Versión del servidor: 5.5.49-0+deb8u1
-- Versión de PHP: 5.6.22-0+deb8u1

INSERT INTO `idiomas` (`pk_idioma`, `idioma`, `nombre`) VALUES
(1, 'ES', 'Español'),
(2, 'EN', 'Ingles'),
(3, 'IT', 'Italiano'),
(4, 'FR', 'Francés'),
(5, 'GE', 'Aleman'),
(6, 'PT', 'Portugués ');
