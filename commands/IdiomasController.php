<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

/**
*   Clase que incluye los roles de la aplicación en BD
*   @author Rodrigo Boet
*   @date 28/10/2016
*/
class IdiomasController extends Controller
{
    public function actionMigrate()
    {
        $sql = file_get_contents(Yii::getAlias('@app').'/commands/idiomas.sql');
        Yii::$app->db->createCommand($sql)->execute();
        echo "Se migro con exito";
    }

    public function actionTruncate()
    {
        Yii::$app->db->createCommand("DELETE FROM idiomas")->execute();
        Yii::$app->db->createCommand("ALTER TABLE idiomas auto_increment = 1")->execute();
        echo "Se vacio la tabla con exito";
    }
}