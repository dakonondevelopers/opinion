--
-- Volcado de datos para la tabla `tematica`
--

INSERT INTO `tematica` (`id_tematica`, `titulo`,`activa`) VALUES
(1, 'Otros',1),
(2, 'Deportes',1),
(3, 'Política',1),
(4, 'Moda y Estilo',1),
(5, 'Tecnología',1),
(6, 'Salud y Bienestar',1),
(7, 'Sociedad',1),
(8, 'Internacional',1),
(9, 'Arte y Cultura',1),
(10, 'Educación',1);

