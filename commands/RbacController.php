<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

/**
*   Clase que incluye los roles de la aplicación en BD
*   @author Rodrigo Boet
*   @date 10/09/2016
*/
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        // se crea el permiso del admin
        $admin = $auth->createRole('Admin');
        $admin->description = 'Es el administrador del sitio';
        $auth->add($admin);
        // se crea el permiso del usuario
        $usuario = $auth->createRole('Usuario');
        $usuario->description = 'Es el rol del usuario común';
        $auth->add($usuario);
        echo "Se crearon los roles con exito";
    }
}