<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\OpinStat;
use app\models\Opin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Query;

/**
 * UserController implements the CRUD actions for User model.
 */
class RankingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],

        ];
    }

    /**
     * Obtiene todas las estadisticas para los ranking.
     * @return mixed
     */
    public function actionIndex()
    {
        //Consulta base de usuarios
        $user = User::find()->joinWith(['fkAuthAssignment']);
        //Consulta base de opin stat
        $opin_stat = OpinStat::find();
        //Usuarios activos e inactivos
        $activo = $user->where(['auth_assignment.item_name'=>'Usuario','status'=>10])->count();
        $inactivo = $user->where(['auth_assignment.item_name'=>'Usuario','status'=>0])->count();
        //Consulta de cuantas opin existen
        $opin = Opin::find()->count();
        //Consulta de cuantos votos se han realizado
        $votos = $opin_stat->count();
        //Consulta de opin mas votada
        $opin_mas_votada = $this->opin_mas_votada()['number'];
        if(!$opin_mas_votada)
        {
            $opin_mas_votada = 0;
        }
        //Consulta Top 10 de Usuarios
        $top_10_usuario = $this->top_ten_usuario();
        //Consulta Top 10 de Paises
        $top_10_pais = $this->top_ten_pais();
        //Consulta Top 10 de Opin
        $top_10_opin = $this->top_ten_opin();
        return $this->render('index',[
            'activo' => $activo,
            'inactivo' => $inactivo,
            'opin' => $opin,
            'votos' => $votos,
            'opin_mas_votada' => $opin_mas_votada,
            'top_10_usuario' => $top_10_usuario,
            'top_10_opin' => $top_10_opin,
            'top_10_pais' => $top_10_pais,
            ]);
    }

    /**
     * Método para obtener la cantidad de respuestas de la opin más votada
     * @author Rodrigo Boet
     * @date 04/12/2016
     * @return array con el parametro number con el numero de respuestas de esa opin
     */
    public function opin_mas_votada()
    {
        //Query para obtener el la cantidad de votos de la primera opin con más respuestas
        $query = new Query;
        $query->select('COUNT(fk_opin) AS number')
            ->from('opin_stat')
            ->groupBy(['fk_opin'])
            ->orderBy(['number'=>SORT_DESC]);
         return $query->one();
    }

    /**
     * Método para obtener los 10 usuarios que más han votado en Opins
     * @author Rodrigo Boet
     * @date 04/12/2016
     * @return array con los parametros [usuario, total de votos ]
     */
    public function top_ten_usuario()
    {
        //Query para obtener los diez usuarios con más participacion en opin
        $query = new Query;
        $query->select(['username','COUNT(fk_user) AS votos'])
            ->from('opin_stat')
            ->innerJoin('user')
            ->where('opin_stat.fk_user = user.id')
            ->groupBy('username')
            ->limit(10)
            ->orderBy(['votos'=>SORT_DESC]);
        return $query->all();
    }

    /**
     * Método para obtener los 10 paises con más usuarios activos
     * @author Rodrigo Boet
     * @date 04/12/2016
     * @return array con los parametros [nombre pais, total de usuarios ]
     */
    public function top_ten_pais()
    {
        //Query para obtener los paises con mayor cantidad de usuarios activos
        //y cuyo rol no sea Admin
        $query = new Query;
        $query->select(['nombre_pais','COUNT(fk_user) AS cantidad'])
            ->from('perfil')
            ->innerJoin('pais')
            ->innerJoin('auth_assignment')
            ->innerJoin('user')
            ->where('pais.id_pais = perfil.fk_pais')
            ->andWhere('auth_assignment.user_id = perfil.fk_user')
            ->andWhere('user.id = perfil.fk_user')
            ->andWhere('auth_assignment.item_name = "Usuario"')
            ->andWhere('user.status = 10')
            ->groupBy('nombre_pais')
            ->limit(10)
            ->orderBy(['cantidad'=>SORT_DESC]);
        return $query->all();
    }

    /**
     * Método para obtener las 10 opin con más participación
     * @author Rodrigo Boet
     * @date 04/12/2016
     * @return array con los parametros [titulo, cantidad de respuestas ]
     */
    public function top_ten_opin()
    {
        //Query para obtener las diez opin con más respuestas
        $query = new Query;
        $query->select(['titulo','COUNT(fk_opin) AS respuestas'])
            ->from('opin_stat')
            ->innerJoin('opin')
            ->where('opin_stat.fk_opin = opin.id_opin')
            ->groupBy('titulo')
            ->limit(10)
            ->orderBy(['respuestas'=>SORT_DESC]);
        return $query->all();
    }
}