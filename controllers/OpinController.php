<?php

namespace app\controllers;

use Yii;
use app\models\Opin;
use app\models\OpinSearch;
use app\models\OpinOpcion;
use app\models\Tematica;
use app\models\MultipleModel;
use app\models\Perfil;
use app\models\OpinPrivada;
use app\models\OpinTematica;
use app\forms\OpinForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * OpinController implements the CRUD actions for Opin model.
 */
class OpinController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create',
                        'update','delete','activate','desactivate'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Opin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OpinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Opin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'opcion' => $model->opinOpcions,
        ]);
    }

    /**
     * Creates a new Opin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OpinForm();
        $modelsOpcion = [new OpinOpcion,new OpinOpcion,new OpinOpcion];
        $tematica = Tematica::find()->where(['activa'=>1])->asArray()->all();

        if ($model->load(Yii::$app->request->post())) {
            $modelsOpcion = MultipleModel::createMultiple(OpinOpcion::classname());
            MultipleModel::loadMultiple($modelsOpcion, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            $user = Perfil::find()->where(['fk_user'=>Yii::$app->user->id])->one();
            $model->fk_user = $user->fk_user;
            $model->fk_idioma = $user->fkPais->idioma;
            $model->fecha_creacion = date('Y-m-d');
            //Se manipula la imagen (si existe)
            $ui = \yii\web\UploadedFile::getInstance($model, 'opin_foto');
            if($ui)
            {
                $name = Yii::$app->user->id.Yii::$app->security->generateRandomString().'.'.split('/',$ui->type)[1];
                $ui->saveAs(Yii::getAlias('@webroot') . '/uploads/ ' . $name);
                $model->opin_foto = '/uploads/ ' . $name;
            }
            if($model->validate())
            {
                $opin = new Opin();
                $this->copyValues($opin,$model);
                try {
                    if ($flag = $opin->save(false)) {
                        //Si la sala es privada se genera el token de forma automática
                        if(!$opin->publica)
                        {
                            $privada = new OpinPrivada();
                            $privada->token = Yii::$app->user->id.Yii::$app->security->generateRandomString();
                            $privada->fk_opin = $opin->id_opin;
                            $privada->save();
                        }
                        //Se crea la tematica
                        $this->getTematica($model->tematica,$opin->id_opin);
                        foreach ($modelsOpcion as $opcion) {
                            $opcion->fk_opin = $opin->id_opin;
                            if (! ($flag = $opcion->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $opin->id_opin]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            if($ui)
            {
                unlink(Yii::getAlias('@webroot') . '../uploads/' . $name);
            }
        } 
        return $this->render('create', [
            'model' => $model,
            'modelsOpcion' => (empty($modelsOpcion)) ? [new OpinOpcion] : $modelsOpcion,
            'tematica' => $tematica,
        ]);
    }

    /**
     * Función para crear los modelos de temáticas
     * @param $tematica Recibe un arreglo con los id de las temáticas
     * @param $id_opin Recibe el id del opin con el que se relacionará
     * @author Rodrigo Boet
     * @date 07/12/2016
     */
    public function getTematica($tematica,$id_opin)
    {
        //Si no envian nada, se crea la tematica con valor "Otros"
        //por defecto
        if(!$tematica)
        {
            $model = new OpinTematica();
            $model->fk_tematica = 1;
            $model->fk_opin = $id_opin;
            $model->save();
        }
        //Si envian valores
        else
        {
            foreach ($tematica as $key => $value) {
                $model = new OpinTematica();
                $model->fk_tematica = $tematica[$key];
                $model->fk_opin = $id_opin;
                $model->save();
            }
        }
    }

    /**
     * Función para copiar los valores del modelo de opin al form y viceversa
     * @param $to Recibe el objeto donde va a copiar los elementos
     * @param $from Recibe el objeto de donde se copiaran los elementos
     * @author Rodrigo Boet
     * @date 07/12/2016
     */
    public function copyValues($to,$from)
    {
        $to->fk_user = $from->fk_user;
        $to->seleccion = $from->seleccion;
        $to->fk_idioma = $from->fk_idioma;
        $to->titulo = $from->titulo;
        $to->opin = $from->opin;
        $to->fecha_creacion = $from->fecha_creacion;
        $to->fecha_fin = $from->fecha_fin;
        $to->relevancia = $from->relevancia;
        $to->opin_foto = $from->opin_foto;
        $to->activo = $from->activo;
        $to->publica = $from->publica;
    }

    /**
     * Updates an existing Opin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $opin = $this->findModel($id);
        $model = new OpinForm();
        $this->copyValues($model,$opin);
        $modelsOpcion = $opin->opinOpcions;
        $tematica = Tematica::find()->where(['activa'=>1])->asArray()->all();

        if ($model->load(Yii::$app->request->post())) {
            $ui = \yii\web\UploadedFile::getInstance($model, 'opin_foto');
            if($ui)
            {
                unlink(Yii::getAlias('@webroot') . $model->opin_foto);
                $name = Yii::$app->user->id.Yii::$app->security->generateRandomString().'.'.split('/',$ui->type)[1];
                $ui->saveAs(Yii::getAlias('@webroot') . '/uploads/ ' . $name);
                $model->opin_foto = '/uploads/ ' . $name;
            }
            $oldIDs = ArrayHelper::map($modelsOpcion, 'id_opcion', 'id_opcion');
            $modelsOpcion = MultipleModel::createMultiple(OpinOpcion::classname(), $modelsOpcion);
            MultipleModel::loadMultiple($modelsOpcion, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsOpcion, 'id_opcion', 'id_opcion')));
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    foreach ($modelsOpcion as $opcion) {
                        $opcion->fk_opin = $model->id_opin;
                        if (! ($flag = $opcion->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }
                }
                if ($flag) {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id_opin]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'opin' => $opin,
                'modelsOpcion' => (empty($modelsOpcion)) ? [new OpinOpcion] : $modelsOpcion,
                'tematica' => $tematica,
            ]);
        }
    }

    /**
     * Función para eliminar tematicas relacionadas con una opin
     * @author Rodrigo Boet
     * @date 07/12/2016
     * @param $model Recibe el modelo del opin
     * @param $tematicas Recibe el array de las tematicas
     */
    public function deleteTematica($model,$tematicas)
    {
        foreach ($model->opinTematicas as $key => $value) {
            if(!in_array($value->fk_tematica, $tematicas))
            {
                $value->delete();
            }
        }
        //Si se borraron todas las temáticas se agrega la por defecto
        if(!$model->opinTematicas)
        {
            $tematica = new OpinTematica();
            $tematica->fk_tematica = 1;
            $tematica->fk_opin = $model->id_opin;
            $tematica->save();
        }
        else
        {
            $keys = array_keys(ArrayHelper::map($model->opinTematicas, 'fk_tematica', 'fk_tematica'));
            $values = array_diff($tematicas, $keys);
            foreach ($values as $value) {
                $tematica = new OpinTematica();
                $tematica->fk_tematica = $value;
                $tematica->fk_opin = $model->id_opin;
                $tematica->save();
            }
        }
    }

    /**
     * Función que permite activar una encuesta
     * @author Rodrigo Boet
     * @date 29/10/2016
     * @param $id Recibe el id de la encuesta
     * @return Regresa a la vista del index
    */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        if($model)
        {
            $model->activo = 1;
            $model->save();
            Yii::$app->session->setFlash('success','Se activó la encuesta con éxito');
        }
        else
        {
            Yii::$app->session->setFlash('error','La encuesta solicitada no existe');
        }
        return $this->redirect(['index']);
    }

    /**
     * Función que permite desactivar una encuesta
     * @author Rodrigo Boet
     * @date 29/10/2016
     * @param $id Recibe el id de la encuesta
     * @return Regresa a la vista del index
    */
    public function actionDesactivate($id)
    {
        $model = $this->findModel($id);
        if($model)
        {
            $model->activo = 0;
            $model->save();
            Yii::$app->session->setFlash('success','Se desactivó la encuesta con éxito');
        }
        else
        {
            Yii::$app->session->setFlash('error','La encuesta solicitada no existe');
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Opin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Opin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Opin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Opin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
