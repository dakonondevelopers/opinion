<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Perfil;
use app\models\Pais;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\forms\RegisterForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create',
                        'update','delete','activate','desactivate','getmails'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new RegisterForm();
        $data = Yii::$app->request->post();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model = new User();
            $perfil = new Perfil();
            $model->username = $form->username;
            $model->email = $form->email;
            $model->setPassword($form->password);
            $model->generateAuthKey();
            $model->save();
            $perfil->telefono = $form->telefono;
            $perfil->genero = $form->genero;
            $perfil->edad = $form->edad;
            $perfil->fecha_nacimiento = $form->fecha_nacimiento;
            $perfil->fk_pais = $form->pais;
            $perfil->fk_user = $model->id;
            $perfil->save();
            //Se asigna el rol del usuario
            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole($data['roles']);
            $auth->assign($authorRole, $model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $pais = Pais::find()->orderBy('nombre_pais ASC')->all();
            return $this->render('create', [
                'model' => $form,
                'pais'=>$pais,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $perfil = $model->fkPerfil;
        $form = new RegisterForm();
        $form->username = $model->username;
        $form->email = $model->email;
        $form->password = $model->password_hash;
        $form->telefono = $perfil->telefono;
        $form->genero = $perfil->genero;
        $form->edad = $perfil->edad;
        $form->fecha_nacimiento = $perfil->fecha_nacimiento;
        $form->pais = $perfil->fk_pais;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $pais = Pais::find()->orderBy('nombre_pais ASC')->all();
            return $this->render('update', [
                'model' => $model,
                'form' => $form,
                'pais'=>$pais,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Función que permite activar un usuario
     * @author Rodrigo Boet
     * @date 12/11/2016
     * @param $id Recibe el id del usuario
     * @return Regresa a la vista del index
    */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        if($model)
        {
            $model->status = 10;
            $model->save();
            Yii::$app->session->setFlash('success','Se activó el usuario con éxito');
        }
        else
        {
            Yii::$app->session->setFlash('error','El usuario solicitado no existe');
        }
        return $this->redirect(['index']);
    }

    /**
     * Función que permite desactivar un usuario
     * @author Rodrigo Boet
     * @date 12/11/2016
     * @param $id Recibe el id de la encuesta
     * @return Regresa a la vista del index
    */
    public function actionDesactivate($id)
    {
        $model = $this->findModel($id);
        if($model)
        {
            $model->status = 0;
            $model->save();
            Yii::$app->session->setFlash('success','Se desactivó el usuario con éxito');
        }
        else
        {
            Yii::$app->session->setFlash('error','El usuario solicitado no existe');
        }
        return $this->redirect(['index']);
    }

    /**
     * Función que permite generar un csv con los nombre de usuario y correo
     * @author Rodrigo Boet
     * @date 03/12/2016
     * @return Regresa el csv, o redirecciona la index si no existen usuarios con rol normal
    */
    public function actionGetmails()
    {
        $model = User::find()->joinWith(['fkAuthAssignment'])
        ->where(['auth_assignment.item_name'=>'Usuario','status'=>10])->all();
        if($model)
        {
            $content = \yii\helpers\ArrayHelper::map($model, 'username', 'email');
            return Yii::$app->response->sendContentAsFile($this->renderPartial("mails",['content'=>$content]),'mail.csv');
        }
        Yii::$app->session->setFlash('warning','No existen Usuarios con el rol normal para generar el csv.');
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
